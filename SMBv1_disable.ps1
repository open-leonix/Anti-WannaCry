# SMB v1  disable from windows 7 or more. Please report info@leonix.in if not working with any version.
#Thanks Certfr
#- Alphalykos
 
If ([Environment]::OSVersion.Version -ge (new-object 'Version' 10,0))
{
  Disable-WindowsOptionalFeature -FeatureName SMB1Protocol -Online
}
else
{
  Set-Service mrxsmb10 -StartupType Disabled
  $Svc = Get-WmiObject win32_Service -filter "Name = 'LanmanWorkstation'"
  $Svc.Change($null, $null, $null, $null, $null, $null, $null, $null, $null, $Null, @( 'Bowser', 'MRxSmb20','CLF'))
  $RegKey = 'HKLM:\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters'
  If ((Test-Path -Path $RegKey) -and (((Get-ItemProperty -LiteralPath $RegKey).psbase.members | % {$_.Name}) -contains 'SMB1'))
  {
    Set-ItemProperty -Path $RegKey -Name SMB1 -Value 0
  }
}
